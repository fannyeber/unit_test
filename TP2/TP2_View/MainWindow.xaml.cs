﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace TP2_View
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }
        
        /**
         * VALIDATION DES TEXTBOX 
         */
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void DecimalValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void btn_calculate_Click(object sender, RoutedEventArgs e)
        {
            var montant = txt_montant.Text;
            var taux = txt_taux_annuel.Text;
            var duree = txt_duree_mois.Text;
            var isSportif = box_sportif.IsChecked;
            var isFumeur = box_fumeur.IsChecked;
            var isTroublesCardiaques = box_cardiaque.IsChecked;
            var isIngenieurInfo = box_inge.IsChecked;
            var isPiloteChasse = box_pilote.IsChecked;

            var mensualite = Calculatrice.CalculerMensualite(Convert.ToDouble(montant), Convert.ToDouble(taux), Convert.ToInt32(duree));

        }
    }
}
