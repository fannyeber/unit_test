﻿namespace TP2
{
    public class Calculatrice
    {
        public static double CalculerMensualite(double montant, double taux, int duree)
        {
            var tauxMensuel = taux / 12;
            return Math.Round(((montant * tauxMensuel) / (1 - Math.Pow(1 + tauxMensuel, -duree))),2,MidpointRounding.ToZero);
        }
        
        public static double CalulerMensualiteAssurance(double montant, double tauxAssurance)
        {
            return montant * (tauxAssurance /12);
        }

        public static double CalculerTotalMensualite(double mensualite, double mensualiteAssurance)
        {
            return mensualite + mensualiteAssurance;
        }

        public static double CalculTotalInteret(double mensualiteTotale, double montant, int duree)
        {
            return mensualiteTotale * duree - montant;
        }

        public static double TauxAssuranceSimple(bool sportif, bool fumeur, bool troubles_cardiaques, bool ingenieur_info, bool pilote_chasse)
        {
            var taux = 0.003;
            if (sportif)
            {
                taux -= 0.0005;
            }
            if (fumeur)
            {
                taux += 0.0015;
            }
            if (troubles_cardiaques)
            {
                taux += 0.003;
            }
            if (ingenieur_info)
            {
                taux -= 0.0005;
            }
            if (pilote_chasse)
            {
                taux += 0.0015;
            }
            return Math.Round(taux, 4);
        }
    }
}