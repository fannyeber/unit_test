﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TP2;

namespace TP2_Interface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        double tauxAssurance = 0;
        
        public MainWindow()
        {
            InitializeComponent();
            display_Taux_Assurance();
        }

        /**
         * VALIDATION DES TEXTBOX 
         */
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void DecimalValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9,]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void btn_calculate_Click(object sender, RoutedEventArgs e)
        {
            var montant = Convert.ToDouble(txt_montant.Text);
            var taux = Convert.ToDouble(txt_taux_annuel.Text);
            var duree = Convert.ToInt32(txt_duree_mois.Text);           


            var mensualite = Calculatrice.CalculerMensualite(montant, taux, duree);
            var mensualiteAssurance = Calculatrice.CalulerMensualiteAssurance(montant, tauxAssurance);
            var totalMensualite = Calculatrice.CalculerTotalMensualite(mensualite, mensualiteAssurance);

            lbl_mensualite.Content = $"Mensualité : {mensualite}";
            lbl_mensualite_assurance.Content = $"Mensualité assurance : {mensualiteAssurance}";
            lbl_mensualite_totale.Content = $"Total mensualité : {totalMensualite}";
            lbl_total_interet.Content = $"Total intérêt : {Calculatrice.CalculTotalInteret(totalMensualite, montant, duree)}";
        }

        private void box_Checked(object sender, RoutedEventArgs e)
        {
            var isSportif = box_sportif.IsChecked ?? false;
            var isFumeur = box_fumeur.IsChecked ?? false;
            var isTroublesCardiaques = box_cardiaque.IsChecked ?? false;
            var isIngenieurInfo = box_inge.IsChecked ?? false;
            var isPiloteChasse = box_pilote.IsChecked ?? false;

            display_Taux_Assurance(isSportif, isFumeur, isTroublesCardiaques, isIngenieurInfo, isPiloteChasse);
        }

        private void display_Taux_Assurance(bool isSportif = false, bool isFumeur = false, bool isTroublesCardiaques = false, bool isIngenieurInfo=false , bool isPiloteChasse=false)
        {
            tauxAssurance = Calculatrice.TauxAssuranceSimple(isSportif, isFumeur, isTroublesCardiaques, isIngenieurInfo, isPiloteChasse);
            lbl_taux_assurance.Content = $"Taux d'assurance : {tauxAssurance}";
        }
    }
}
