using TP2;

namespace TP2_Test
{
    public class CalculatriceTest
    {
        [Theory]
        [InlineData(50000, 0.021, 108, 508.49)]
        [InlineData(50000, 0.0245, 300, 223.05)]
        [InlineData(100000, 0.021, 108, 1016.98)]
        public void ShouldReturnMensualiteWithMinMontant(double montant, double taux, int duree, double expected)
        {
            Assert.Equal(expected, Calculatrice.CalculerMensualite(montant, taux, duree));
        }

        [Theory]
        [InlineData(false, false, false, false, false, 0.003)]
        [InlineData(true, false, false, false, false, 0.0025)]
        [InlineData(false, true, false, false, false, 0.0045)]
        [InlineData(false, false, true, false, false, 0.006)]
        [InlineData(false, false, false, true, false, 0.0025)]
        [InlineData(false, false, false, false, true, 0.0045)]
        [InlineData(true, true, true, true, true, 0.008)]
        public void ShouldReturnTauxAssurance(bool sportif, bool fumeur, bool troubles_cardiaques, bool ingenieur_info, bool pilote_chasse, double expected)
        {
            Assert.Equal(expected, Calculatrice.TauxAssuranceSimple(sportif, fumeur, troubles_cardiaques, ingenieur_info, pilote_chasse));
        }

        [Fact]
        public void ShouldReturnMensualiteAssurance()
        {
            Assert.Equal(125, Calculatrice.CalulerMensualiteAssurance(50000, 0.03));
        }

        [Fact]
        public void ShouldCalulTotalMensualite()
        {
            Assert.Equal(633.49, Calculatrice.CalculerTotalMensualite(508.49, 125));
        }

        [Fact]
        public void ShouldCalculTotalInteret()
        {
            var res = Calculatrice.CalculTotalInteret(500, 50000, 120);
            Assert.Equal(10000, res);
        }

    }
}