﻿

namespace TP1
{
    public class FizzBuzz
    {
        public string Generer(int n)
        {
            if (n < 15 || n > 150)
            {
                throw new ArgumentOutOfRangeException();
            }

            var res = "";
            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    res += "FizzBuzz ";
                }
                else if (i % 3 == 0)
                {
                    res += "Fizz ";
                }
                else if (i % 5 == 0)
                {
                    res += "Buzz ";
                }
                else
                {
                    res += i + " ";
                }
            }
            return res;
        }
    }
}
