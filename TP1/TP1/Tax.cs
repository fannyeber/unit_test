﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    public class Tax
    {
        public string CalculateTax(double amount)
        {

            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            var coef = 0.0;
            if (amount > 17777 && amount <= 27478)
            {
                coef = 0.11;
            }
            else if (amount <= 78570)
            {
                coef = 0.30;
            }
            else if (amount <= 168994)
            {
                coef = 0.41;
            }
            else
            {
                coef = 0.45;
            }
            return $"Taux d'imposition : {coef}\nMontant à payer : ${coef * amount}";
        }
    }
}
