﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP1;

namespace TP1Test
{
    public class FizzBuzzTest
    {
        [Fact]
        public void TestGenerateWithNumberOutOfBoundsThenError()
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            Assert.Throws<ArgumentOutOfRangeException>(() => fizzBuzz.Generer(14));
            Assert.Throws<ArgumentOutOfRangeException>(() => fizzBuzz.Generer(151));
        }

        [Theory]
        [InlineData(15, 4, 2, 1)]
        [InlineData(30, 8, 4, 2)]
        [InlineData(150, 40, 20, 10)]
        public void TestGenerate(int n, int expectedNumberFizz, int expectedNumberBuzz, int expectedNumberFizzBuzz)
        {
            FizzBuzz fizzBuzz = new FizzBuzz();

            var res = fizzBuzz.Generer(n);
            var numberFizz = res.Split(' ').Where(x => x == "Fizz").Count();
            var numberBuzz = res.Split(' ').Where(x => x == "Buzz").Count();
            var numberFizzBuzz = res.Split(' ').Where(x => x == "FizzBuzz").Count();
                        
            Assert.Equal(expectedNumberFizz, numberFizz);
            Assert.Equal(expectedNumberBuzz, numberBuzz);
            Assert.Equal(expectedNumberFizzBuzz, numberFizzBuzz);
        }
    }
}
