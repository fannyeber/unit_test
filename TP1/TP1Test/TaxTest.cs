﻿

using TP1;

namespace TP1Test
{
    public class TaxTest
    {
        [Fact]
        public void TestCalculateTaxWithAmountOutOfBoundsThenError()
        {
            Tax tax = new Tax();
            Assert.Throws<ArgumentOutOfRangeException>(() => tax.CalculateTax(-1));
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(10777, 0)]
        [InlineData(27478, 0.11)]
        [InlineData(78570, 0.30)]
        [InlineData(168994, 0.41)]
        [InlineData(168995, 0.45)]
        public void TestCalculateTax(double amount, double expectedCoef)
        {
            Tax tax = new Tax();
            Assert.Contains(expectedCoef.ToString(), tax.CalculateTax(amount));
            Assert.Contains((amount*expectedCoef).ToString(), tax.CalculateTax(amount));
        }
    }
}
