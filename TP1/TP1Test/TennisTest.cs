﻿

using TP1;

namespace TP1Test
{
    public class TennisTest
    {
        [Fact]
        public void TestScoreWithParamOutOfRange()
        {
            Tennis tennis = new Tennis();
            Assert.Throws<ArgumentOutOfRangeException>(() => tennis.Score(-1, -1));
        }

        [Theory]
        [InlineData(1, 0, "15-0")]
        [InlineData(2, 1, "30-15")]
        [InlineData(0,3, "Winner : player 2")]
        [InlineData (3, 1, "Winner : player 1")]
        [InlineData(4, 5, "40-ADV")]
        [InlineData(5, 5, "40-40")]
        [InlineData (4, 6, "Winner : player 2")]
        public void TestScore(int player1, int player2, string expectedScore)
        {
            Tennis tennis = new Tennis();
            Assert.Equal(expectedScore, tennis.Score(player1, player2));
        }
    }
}
